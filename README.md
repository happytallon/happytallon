# Hi there! I'm Caue Marcovich! :video_game: 

I'm a game programmer/developer from São Paulo, Brazil. 

Here, you will find some of my game projects. Feel free to clone them. :slightly_smiling_face:

If you have any questions, please don't hesitate to reach out.

---
[![Itch.io](https://img.shields.io/badge/Itch.io-FA5C5C?style=for-the-badge&logo=itch.io&logoColor=white)][itchio]
[![Linkedin](https://img.shields.io/badge/Linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)][linkedin]
[![Portfolio](https://img.shields.io/badge/Portfolio-333333?style=for-the-badge)][portfolio]
[![GitHub](https://img.shields.io/badge/GitHub-181717?style=for-the-badge&logo=github&logoColor=white)][github]

----

## Games

#### [<img src="https://gitlab.com/uploads/-/system/project/avatar/10821366/ship_t.png" width=40px/> Back in the Loop][backintheloop]

#### [<img src="https://gitlab.com/uploads/-/system/project/avatar/52024135/1.png" width=40px/> 2B-Runner][2brunner]

#### [<img src="https://img.itch.zone/aW1nLzE2NDEyMTgyLnBuZw==/315x250%23c/MM5m7Z.png" width=40px /> WormsSurvivor][wormssurvivor]

#### [<img src="https://gitlab.com/uploads/-/system/project/avatar/8182280/brick-breaker.png" width=40px/> Brick Breaker][brickbreaker]


[itchio]: https://happytallon.itch.io/
[linkedin]: https://linkedin.com/in/caue-marcovich
[portfolio]: https://cauemarcovich.github.io
[github]: https://gitlab.com/happytallon

[backintheloop]: https://gitlab.com/tallonic_personal/back-in-the-loop
[2brunner]: https://gitlab.com/tallonic_personal/2brunner
[wormssurvivor]: https://github.com/dssluisgustavo/WormSurvivor
[brickbreaker]: https://gitlab.com/tallonic_personal/Brick_Breaker